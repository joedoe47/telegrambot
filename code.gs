// Aloe Joe Bot (joedoe47)
////
// Base Script by: Wim's Coding Secrets
// Source/Video: https://www.youtube.com/watch?v=24EyItKfm50
////
var root_url = "https://init.joepcs.com"
var token = "585363330:AAF7VfC4gRXCJphPO4hzNKESGI-siCwAzDw"; // FILL IN YOUR OWN TOKEN
var telegramUrl = "https://api.telegram.org/bot" + token;
var webAppUrl = "https://script.google.com/macros/s/AKfycbwdNSZufmOIzkPJCXlwKPrMMUgPq6Pi8cVJo-x-bkE8Q5QvCFE/exec"; // FILL IN YOUR GOOGLE WEB APP ADDRESS

// FILL IN THE ID OF YOUR DRIVE SPREADSHEET
var sheet_db = "1NngsizTKgHZ3VFZjDXGOLWGZMX_uFVVp0mEBMSmaYLI"; 

//API calls
////
function getMe() {
  var url = telegramUrl + "/getMe";
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}

function getUpdates() {
  var url = telegramUrl + "/getUpdates";
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}

function setWebhook() {
  var url = telegramUrl + "/setWebhook?url=" + webAppUrl;
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}

function delWebhook() {
  var url = telegramUrl + "/setWebhook";
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}

function sendText(id,text,arg) {
  //default: just send via API (use "arg" to send additional API)
  var url = telegramUrl + "/sendMessage?chat_id=" + id + "&text=" + encodeURIComponent(text) + arg;
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}


//Command Bus
////

//Test Reply
function getTestReply(id, text) { 
  sendText(id,text);
}

//Tell Time
function getTime(id) {
  var date = new Date();
  var currentTime = Utilities.formatDate(date, 'UTC', 'MMMM dd, yyyy HH:mm:ss Z');
  sendText(id,currentTime);
}

/*
# Future proofing

- function "add video" -rename-> "appendsheet"
- function "newvideo" -create- we can do regex here to check for youtube urls
- function "readsheed" -create- we can make other plug ins that read from db
- function "updatesheet" -create- there are other plugins who need to find/replace content

# TODO

- consider making seperate file/functions to minimize file size. (ie. an proper message bus)

# References

- read/find in sheet, https://stackoverflow.com/questions/11095888/search-whole-spreadsheet-to-find-text-within-a-cell-in-a-google-spreadsheet-and
- read/find in sheet, https://stackoverflow.com/questions/18482143/google-scripts-search-spreadsheet-by-column-return-rows

*/

//Add video(s) to spreadsheet
function addVideo(id,text){
//  var ss = SpreadsheetApp.openById(video_db);
//  var sheet = ss.getSheets()[0];
  sheetName="videos"
  var sheet = SpreadsheetApp.openById(sheet_db).getSheetByName(sheetName) ? SpreadsheetApp.openById(sheet_db).getSheetByName(sheetName) : SpreadsheetApp.openById(sheet_db).insertSheet(sheetName);
  
  var comment = text.split(" ").slice(1).join(" ");
  sheet.appendRow([new Date(),id,comment]);
  
  var reply = "Content " + comment + " has been added!"
  sendText(id,reply);
  /* Method to explicitly detect valid youtube URLs not just random text (useful later on)
  var searchText = text
  var urls = searchText.match(/\b(http|https)?(:\/\/)?(\S*)\.(\w{2,4})\b/ig);

  for (var i = 0, il = urls.length; i < il; i++) {
    // Appends a new row for each url
    //Date, Chat_ID, http(s)://youtube.com/watch?v=OtQqy_Aon-E
    sheet.appendRow([new Date(),id,urls[i]]);
  }
  */
}

//Quote (from sample)
function getQuote(id){
  var url = 'http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1';
  var quote_data = UrlFetchApp.fetch(url);
  var posts = JSON.parse(quote_data);
  var post = posts.shift();
        
  // Delete the html tags and \n (newline)
  var cleanContent = post.content.replace(/<(?:.|\n)*?>/gm, "").replace(/\n/gm, "");
        
  // Format the quote
  var quote = '"' + cleanContent + '"\n — <strong>' + post.title + '</strong>';
  sendText(id,quote);
}

// Default response over HTTP(S)
////
function doGet(e) {
  return HtmlService.createHtmlOutput("<script>window.top.location.href='" + root_url + "';</script>");
}

//Telegram Main function
////
function doPost(e) {
  try {
    //Get json from webhook
    var data = JSON.parse(e.postData.contents);
    
    //Data from JSON to be sent to bus
    var chat_text = data.message.text;
    var chat_id = data.message.chat.id;
    var user_name = data.message.chat.first_name;
    
    //test reply to any response
    //getTestReply(chat_id, user_name, chat_text);
    

    if (chat_text == '/time') { getTime(chat_id); }  
    
    if (chat_text == "/test") { getTestReply(chat_id, "wee woo! it werks!"); }
    
    if (chat_text == "/help"){
      getTestReply(chat_id, "I would help but I'm a smol bot");
    }
    
    //regular expression to match '/video [some url]' not just '/video'
    if (/^\/video/.test(chat_text)) {
      addVideo(chat_id,chat_text);
    }
    
    if (chat_text == '/quote') {
      getQuote(chat_id);
    }
    
    //if(/^@/.test(chat_text)) { addVideo(chat_text); } 
    
    
    //}  
    //}
  }catch(e) { sendtext(JSON.stringify(chat_id,e,null,4)); } //display json upon error
  
}
